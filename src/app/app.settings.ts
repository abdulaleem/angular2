export class AppSettings {  
    public static get API_ENDPOINT(): string {
        return  'https://nodefydev.azurewebsites.net'; 
    } 
}

export class AuthProviders {  
    public static get FACEBOOK(): string {
        return  'facebook'; 
    } 

    public static get GOOGLE(): string {
        return  'google'; 
    }

    public static get AAD(): string {
        return  'aad'; 
    }
}