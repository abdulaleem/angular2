import {Component, ViewEncapsulation} from '@angular/core'; 
import { Router, ROUTER_DIRECTIVES} from '@angular/router';

import { AppSettings, AuthProviders } from '../app.settings'; 
declare var WindowsAzure: any;

@Component({
  directives: [
    ROUTER_DIRECTIVES,
  ],
  selector: '[login-welcome]',
  host: {
    class: 'login-welcome-page app'
  },
  template: require('./login.html'),
  encapsulation: ViewEncapsulation.None,
  styles: [require('./login.scss')]
})
export class LoginWelcomePage {

  client : any;

  constructor(public _router : Router ) {  
  }

  public FbLogin(){ 
     
      var myrouter = this._router;

      this.client = new WindowsAzure.MobileServiceClient(AppSettings.API_ENDPOINT);

      this.client.login(AuthProviders.FACEBOOK).done(function (data) {

        localStorage.setItem("user", data.userId);
        localStorage.setItem("token", data.mobileServiceAuthenticationToken); 

        console.log(data); 

         myrouter.navigate(['/']);
        
      }, function (err) {
          alert("Error: " + err);
      });

      
  }

}
