import {Component} from '@angular/core';
import { Router, ROUTER_DIRECTIVES} from '@angular/router';

import { AppSettings, AuthProviders } from '../app.settings'; 
 
declare var WindowsAzure: any;

@Component({
  directives: [
    ROUTER_DIRECTIVES,
  ],
  selector: '[login]',
  host: {
    class: 'login-page app'
  },
  template: require('./login.html')
})


export class LoginPage {
  client : any;
  

  constructor(public _router : Router ) { 
    // var store = new WindowsAzure.MobileServiceSqliteStore('store.db');
      console.log('It works here LoginPage') 
      console.log('It works here LoginPage',WindowsAzure) 
  }

  public AdLogin(){
    this.client = new WindowsAzure.MobileServiceClient(AppSettings.API_ENDPOINT);

      this.client.login(AuthProviders.AAD).done(function (results) {
        console.log(results); 
      alert("You are now logged in as: " + results.userId);
      }, function (err) {
          alert("Error: " + err);
      });

  }
 
}
